<?php

/**
 * @file
 */

use Drupal\user\entity\User;

/**
 * Callback function for authorizations available to the user.
 *
 * Required by ldap_authentication.
 *
 * @param \Drupal\user\entity\User $user
 * @param $op
 * @param $profile_id
 *
 * @return array
 */
function _authorizations_user_authorizations(User $user, $op, $profile_id) {
  /* @var \Drupal\authorization\Entity\AuthorizationProfile $profile */
  $profile = \Drupal::entityManager()->getStorage('authorization_profile')->load($profile_id);

  $authorizations = [];
  $notifications = [];

  /**
     * User 1 not used in ldap authorization. This is a design decision.
     * @TODO have this configurable per provider or per profile.
     */
  if ($user->id() == 1) {
    if (\Drupal::config('ldap_help.settings')->get('watchdog_detail')) {
      \Drupal::logger('authorization')->debug('%username : authorization not applied to user 1', ['%username' => $user->getDisplayName()]);
    }
    $notifications['all'] = AUTHORIZATION_NOT_APPLY_USER_1;
    return [$authorizations, $notifications];
  }

  if ($profile->checkConditions($user, $op)) {
    if ($op == 'set') {
      $user_save = TRUE;
    }
    else {
      $user_save = FALSE;
    }
    // Apply the profile map to the user.
    $user_auth_data = [];
    list($authorizations, $notifications) = $profile->grantsAndRevokes($op, $user, $user_auth_data, '', $user_save);
  }

  return [$authorizations, $notifications];

}
